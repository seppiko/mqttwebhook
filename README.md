# Seppiko MQTT webhook

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](LICENSE)

Seppiko MQTT webhook is an EMQX webhook client like backend to mysql

## Features

- [x] EMQX webhook
- [x] Support MySQL or MariaDB

## cli paramater

`-c <config.json>` Load custom config file

`-v` Print version

## `config.json`

```json
{
  "server": ":8080",
  "db": {
    "url": "mysql://127.0.0.1:3306/test",
    "username": "root",
    "password": ""
  }
}
```

server start `:` is `0.0.0.0:`

db `url` is _GDBC_, schema is must be `mysql`. And can not support any parameters.

## `logrus.json`

```json
{
  "type": "stdout",
  "brokers": [
    "127.0.0.1:9092"
  ]
}
```

`type` must is `stdout`, `file` or `kafka` .

`brokers` is kafka node

# DB table

see `mqtthook.sql`

## API

`/webhook` POST

see [Send data to WebHook](https://www.emqx.io/docs/en/v5.0/data-integration/rule-sql-events-and-fields.html)

## Sponsors

<a href="https://www.jetbrains.com/" target="_blank"><img src="https://seppiko.org/images/jetbrains.png" alt="JetBrians" width="100px"></a>
