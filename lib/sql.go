/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
)

func Add(sqlstr string) bool {
  url, err := GetGdbcUrl(dbc)
  if err != nil {
    l.Error(err)
    return false
  }
  db, _ := sql.Open("mysql", url)
  defer db.Close()

  l.Infof("SQL: %s\n", sqlstr)
  prepare, err := db.Exec(sqlstr)
  if err != nil {
    l.Warnf("SQL execute failed. %s", sqlstr)
    l.Warn(err)
    return false
  }

  rows, _ := prepare.RowsAffected()
  if rows > 0 {
    return true
  }
  return false
}
