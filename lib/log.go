/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
  "encoding/json"
  "io"
  "os"
  "time"

  "github.com/Shopify/sarama"
  nested "github.com/antonfisher/nested-logrus-formatter"
  logkafka "github.com/kenjones-cisco/logrus-kafka-hook"
  "github.com/sirupsen/logrus"
)

type UTCFormatter struct {
  logrus.Formatter
}

func (u UTCFormatter) Format(e *logrus.Entry) ([]byte, error) {
  e.Time = e.Time.UTC()
  return u.Formatter.Format(e)
}

type LogrusConfig struct {
  Type    string   `json:"type"`
  Brokers []string `json:"brokers,omitempty"`
}

var (
  fileFormatter = nested.Formatter{
    TimestampFormat: DATETIMEFORMAT,
    HideKeys:        true,
    ShowFullLevel:   true,
    NoColors:        true,
    FieldsOrder:     []string{"component", "category"},
  }

  stdFormatter = nested.Formatter{
    TimestampFormat: DATETIMEFORMAT,
    HideKeys:        true,
    ShowFullLevel:   true,
    NoColors:        false,
    FieldsOrder:     []string{"component", "category"},
  }

  jsonFormatter = logrus.JSONFormatter{
    TimestampFormat: DATETIMEFORMAT,
    FieldMap: logrus.FieldMap{
      logrus.FieldKeyTime:  "@timestamp",
      logrus.FieldKeyMsg:   "message",
      logrus.FieldKeyLevel: "@level",
    },
  }
)

func InitLogger() *logrus.Entry {
  logConf := logrus.New()

  logConf.SetFormatter(&stdFormatter)
  logConf.SetOutput(os.Stdout)

  logFields := logrus.Fields{}

  var conf LogrusConfig
  data, err := LoadConfig("logrus.json")
  if err != nil {
    logConf.Errorf("Config file logrus.json load failed. %s", err.Error())
    return logConf.WithFields(logFields)
  }
  _ = json.Unmarshal([]byte(data), &conf)

  if conf.Type == "stdout" {
  } else if conf.Type == "kafka" {
    logConf.SetFormatter(UTCFormatter{&jsonFormatter})
    logFields = logrus.Fields{
      "@version": GetVersion(),
      "type":     "log",
    }

    hook := logkafka.New()
    producer, _ := logkafka.SimpleProducer(conf.Brokers, sarama.CompressionSnappy, sarama.WaitForLocal, nil)
    hook = hook.WithProducer(producer)
    logConf.AddHook(hook)
  } else if conf.Type == "file" {
    logConf.SetFormatter(&fileFormatter)

    datetime := time.Now().Format(DATEFORMAT)
    path := "logs/"
    if _, err := os.Stat(path); err != nil {
      err = os.MkdirAll(path, 0754)
      if err != nil {
        panic(err)
      }
    }

    lfs, err := os.OpenFile(path+"mqttwebhook_"+datetime+".log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0754)
    if err != nil {
      panic(err)
    }
    //defer lfs.Close()
    mw := io.MultiWriter(os.Stdout, lfs)
    logConf.SetOutput(mw)
  } else {
    logConf.Warn("Logrus type error.")
  }

  return logConf.WithFields(logFields)
}
