/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
	"encoding/json"
	"io/ioutil"
)

// Author: Leonard Woo (https://l6d.me)

const (
	DATETIMEFORMAT = "2006-01-02 15:04:05"
	DATEFORMAT     = "2006-01-02"
)

var (
	l = InitLogger()
)

func LoadConfig(path string) (string, error) {
	buf, err := ioutil.ReadFile(path)
	if err != nil {
		return "", &Error{message: "Can not found config file or create file failed."}
	}
	return string(buf), nil
}

type Config struct {
	Server string `json:"server"`
	DB     *Db    `json:"db"`
}

func ParserJson(data string) Config {
	var conf Config
	err := json.Unmarshal([]byte(data), &conf)
	if err != nil {
		l.Error("JSON parser failed")
	}
	return conf
}
