/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
  "fmt"
  "net/url"
  "strconv"
  "strings"
)

type Error struct {
  message string
}

func (e *Error) Error() string {
  return e.message
}

type Db struct {
  Url      string `json:"url"`
  Username string `json:"username"`
  Password string `json:"password"`
}

func GetGdbcUrl(db *Db) (string, error) {
  u, err := url.Parse(db.Url)
  if err != nil {
    return "", err
  }

  if u.Scheme != "mysql" {
    return "", &Error{message: "scheme is not mysql"}
  }

  h := strings.Split(u.Host, ":")
  hostname := h[0]
  port, _ := strconv.ParseInt(h[1], 0, 64)

  database := u.Path[1:]

  sqlUrl := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", db.Username, db.Password, hostname, port, database)

  return sqlUrl, nil
}
