/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var (
	dbc *Db
	UA  = fmt.Sprintf("Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0 MQTTWebhook/%s", GetVersion())
)

func init() {
}

func Actions(config Config) {
	dbc = config.DB
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/webhook", webhookHandler)
	http.HandleFunc("/metrics", metricsHandler)
}

func indexHandler(w http.ResponseWriter, req *http.Request) {
	//body, err := ioutil.ReadAll(req.Body)
	//if err != nil {
	//  l.Warn(err)
	//}
	//l.Info(string(body))
	//defer req.Body.Close()

	w.WriteHeader(http.StatusBadRequest)
	w.Header().Add("Accept", "application/json")
	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Server", "MqttWebhook/"+GetVersion())
	w.Write([]byte(`{"code":400,"message":"Bad request"}`))
	l.Info(req.Header.Get("User-Agent"))
}

func webhookHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Accept", "application/json")
	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Server", "MqttWebhook/"+GetVersion())

	if req.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte(`{"code":405,"message":"Method Not Allowed"}`))
		return
	}

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		l.Warn(err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"code":400,"message":"Can not got body"}`))
		return
	}
	defer req.Body.Close()

	var bodyJson EmqxWebHook
	err = json.Unmarshal(body, &bodyJson)
	if err != nil {
		l.Warn(err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"code":400,"message":"Json parser failed"}`))
		return
	}

	if !Submit(bodyJson) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"code":400,"message":"Data insert failed"}`))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"code":200,"message":"Successful"}`))
}

func metricsHandler(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Add("Accept", "application/json")
	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Server", "SeppikoMQTTWebhook/"+GetVersion())
	w.Write([]byte(`{"code":200,"message":"ok"}`))
}
