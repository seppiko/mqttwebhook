/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
	"fmt"
	"mqttwebhook/hook"
)

func Submit(json EmqxWebHook) bool {

	var retain = 0
	if json.Flags.Retain {
		retain = 1
	}

	var s = fmt.Sprintf("INSERT INTO mqtthook (timestamp, id, topic, qos, payload, retain, clientid) "+
		"VALUES (%d, \"%s\", \"%s\", %d, \"%s\", %d, \"%s\")",
		json.Timestamp, json.Id, convert(json.Topic), json.Qos, convert(json.Payload), retain, json.Clientid)

	if Add(s) {
		l.Infof("Data (ID: %s) insert successful.", json.Id)
		if !hook.Hook(json.Payload) {
			l.Infof("Hook execute failed.")
		}
		return true
	}
	return false
}

func specialLetters(letter rune) (bool, []rune) {
	if letter == '/' || letter == '"' {
		var chars []rune
		chars = append(chars, '\\', letter)
		return true, chars
	}
	return false, nil
}

func convert(str string) string {
	var chars []rune
	for _, letter := range str {
		ok, letters := specialLetters(letter)
		if ok {
			chars = append(chars, letters...)
		} else {
			chars = append(chars, letter)
		}
	}
	return string(chars)
}

type EmqxWebHook struct {
	Id                string                 `json:"id"`
	Username          string                 `json:"username"`
	Topic             string                 `json:"topic"`
	Timestamp         uint64                 `json:"timestamp"`
	Qos               int8                   `json:"qos"`
	PublishReceivedAt int64                  `json:"publish_received_at"`
	PubProps          map[string]interface{} `json:"pub_props"`
	Peerhost          string                 `json:"peerhost"`
	Payload           string                 `json:"payload"`
	Node              string                 `json:"node"`
	Metadata          Metadata               `json:"metadata"`
	Flags             Flags                  `json:"flags"`
	Event             string                 `json:"event"`
	Clientid          string                 `json:"clientid"`
}

type Metadata struct {
	RuleId string `json:"rule_id"`
}

type Flags struct {
	Retain    bool `json:"retain"`
	Duplicate bool `json:"dup"`
}
