# Seppiko MQTT webhook

## 1.0.4 - 2022-08-31
1. Fixed bug
2. Move hook
3. Delete http request with proxy
4. Update EMQX Webhook struct
5. Supported EMQX 5.0

## 1.0.3 - 2022-01-15
1. Update HTTP client
2. Update SQL
3. Add Metric
4. Change log folder
5. Update copyright

## 1.0.2 - 2021-11-21
1. Update log format
2. Add hook
3. Add Http client
4. Add Json
5. Rename to `main.go`

## 1.0.1 - 2021-10-31
1. Fixed bug
2. Add log file support 

## 1.0.0 - 2021-10-28
1. Json config
2. EMQX Webhook
3. MySQL/MariaDB Client
4. Logrus with Kafka
