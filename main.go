/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"flag"
	"fmt"
	"mqttwebhook/lib"
	"net/http"
	"os"
	"strings"
)

// Author: Leonard Woo

var (
	l            = lib.InitLogger()
	version      = lib.GetVersion()
	printVersion = false
	config       = "config.json"
)

func init() {
	flag.BoolVar(&printVersion, "v", false, "Print Version")
	flag.StringVar(&config, "c", config, "config file")
}

func main() {
	flag.Parse()

	if printVersion {
		fmt.Printf("MQTT Webhook version: %s", version)
		os.Exit(0)
	}

	if len(config) < 1 || !strings.HasSuffix(config, ".json") {
		l.Errorf("Config file load failed.")
		flag.Usage()
		os.Exit(1)
	}

	fmt.Printf("MQTT Webhook %s\n", version)
	conf, _ := lib.LoadConfig(config)
	//fmt.Println( string(conf) )
	config := lib.ParserJson(conf)

	lib.Actions(config)
	l.Infof("Start server at %s", config.Server)
	err := http.ListenAndServe(config.Server, nil)
	if err != nil {
		l.Error(err)
	}

}
